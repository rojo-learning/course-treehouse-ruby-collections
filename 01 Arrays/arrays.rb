
# Arrays

## Creating arrays
array = Array.new
array = []

## Initializing arrays
array = ["milk", "eggs", "bread"]
array = %w(milk eggs bread)

item  = "milk"
array = %W(#{item} eggs bread)

## Add items to the END of the array
array << "carrots"
array.push("potatoes")
array += ["bacon", "tomatoes"]

## Add items to the BEGINNING of the array
array.unshift("bananas")
array = ["lettuce", "beans"] + array

## Accessing items in arrays
### Get the number of items
array.length() # => 10
array.count()  # => 10

## Get items
item = array.at(1) # => "beans"
item = array[0]    # => "lettuce"
item = array.first # => "lettuce"
item = array[-1]   # => "tomatoes"
item = array.last  # => "tomatoes"

### Get certain item, default to something if the item doesn't exist
item = array.fetch(20, "cake") # => "cake"

### Get several items from anywhere
more_items = array.slice(1,2) # => ["beans", "bananas"]

### Get the elements from the array, except for the first n
front_items = array.drop(5) # => ["bread", "carrots", "potatoes", "bacon", "tomatoes"]

### Count matching items
array.count("eggs") # => 1

### Check if certain item exists
array.include?("eggs") # => true

## Remove items
### Last item
last_item = array.pop    # => "tomatoes"
array.last               # => "bacon"

### First item
first_item = array.shift # => "lettuce"
array.first              # => "beans"

### Remove several items from anywhere
more_items = array.slice!(1,2) # => ["bananas", "milk"]
