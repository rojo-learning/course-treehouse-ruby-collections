
# Hashes
# Hashes are collection of key/value pairs.
# Each key references to a value, so each key must be unique.
# Symbols and Strings are usually used as keys, but any object could be a key.
# A value can be any object, even other hashes.
#
# Two hashes are considered equal when they have the same keys and values.

## Hash creation
hashy = Hash.new
hashy = {}

## Initializing a hash
hashy = { item: "Bread", quantity: 1 }

## Accessing values
hashy[:item] # => "Bread"
hashy.fetch(:quantity) # => 1

## Working with keys
### Get the list of keys
hashy.keys() # => [:item, :quantity]

### Check if a key exists
hashy.has_key?(:item)    # => true
hashy.member?(:quantity) # => true
hashy.key?(:price)       # => false

### Add a new key/value pair
hashy.store(:calories, 75)
hashy[:price] = 40.0

## Working with values
### Get the list of values
hashy.values # => ["Bread", 1, 75, 40.0]

### Check if a value exists
hashy.has_value?("Bread") # => true
hashy.value?("Bread")     # => true

### Get the values for several keys
hashy.values_at(:item, :price) # => ["Bread", 40.0]

## Hash methods
### Ge the size
hashy.length # => 4

### Get a transposed version
hashy.invert # => { "Bread" => :item, 1 => :quantity, 75 => :calories, 40.0 => :price }

### Remove the first key/value pair
first = hashy.shift # => { item: "Bread" }

### Add or Update a key/value pair
hashy.merge!({ item: "Pasta" }) # => { quantity: 1, calories: 75, price: 40.0, item: "Pasta" }
