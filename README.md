# Treehouse - Ruby Collections
Exercises and examples from the 'Collections' course, imparted by Jason
Seifer at [Treehouse](http://teamtreehouse.com/).

> You'll level up your Ruby knowledge and start learning about collections. We use two basic data structures to create collections in Ruby: arrays and hashes. These are both examples of "collections" and you’ll work with them a lot in your career as a Ruby programmer.

## Topics
- **Ruby Arrays**: An array is a container of data, similar to a list. Arrays can be used to store many different kinds of data including strings, numbers, and almost any other kind of Ruby object. In this stage, we'll learn how to create and manipulate arrays.

- **Ruby Hashes**: A hash is data structure similar to an array but using named slots. Hashes are used all throughout Ruby programs. In this stage, we'll get comfortable creating and working with hashes in Ruby.

## Project
- **Build a Grocery List Program**: Now that we know how to use arrays and hashes, we're going to build a small program that makes a grocery list for us. We'll combine all of the knowledge we've learned so far including input and output, method definitions, and more.


---
This repository contains portions of code from the TreehouseTeam courses and is included only for reference under _fair use_.
