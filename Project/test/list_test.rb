
require 'minitest/autorun'
require_relative '../lib/list'

describe Kernel do

  describe "#ask_for_list_name" do
    let(:an_input) { "Some input\n" }

    it "returns input without new line" do
      expected = an_input.chomp

      self.stub :gets, an_input do
        ask_for_list_name.must_equal expected
      end
    end
  end

  describe "#ask_for_item_data" do
    let(:an_input) { "1\n" }

    it "returns input as an array" do
      expected = [an_input.chomp, an_input.chomp.to_i]

      self.stub :gets, an_input do
        ask_for_item_data.must_equal expected
      end
    end
  end

  describe "#add_list_item" do
    it 'returns an item Hash object' do
      item_data = ["Milk", 6]
      expected  = { name: item_data[0], quantity: item_data[1] }

      craft_list_item(item_data).must_equal expected
    end
  end

  describe "#create_list" do
    it 'returns a list Hash object' do
      list_name = "Groceries"
      expected  = { name: list_name, items: [] }

      create_list(list_name).must_equal expected
    end
  end

  describe "#print_list" do
    let(:a_list) { { name: 'Test', items: [] } }

    describe 'with an empty list' do
      it 'prints only the name and separators' do
        expected  = "List: #{a_list[:name]}\n"
        expected += ("-" * 80 + "\n") * 2

        proc { print_list(a_list) }.must_output expected
      end
    end

    describe 'with a list that has items' do
      before do
        a_list[:items].push({ name: 'Milk', quantity: 6  })
        a_list[:items].push({ name: 'Eggs', quantity: 12 })
      end

      it 'prints the name, items and separators' do
        expected  = "List: #{a_list[:name]}\n"
        expected += "-" * 80 + "\n"
        expected += "\tItem: Milk\t\t\t Quantity: 6\n"
        expected += "\tItem: Eggs\t\t\t Quantity: 12\n"
        expected += "-" * 80 + "\n"

        proc { print_list(a_list) }.must_output expected
      end
    end

  end

  describe "#print_separator" do
    it "puts the default symbol line if none given" do
      expected = "-" * 80 + "\n"
      proc { print_separator }.must_output expected
    end

    it "puts the given symbol line" do
      symbol = '*'
      expected = symbol * 80 + "\n"
      proc { print_separator(symbol) }.must_output expected
    end
  end

end
