
module Kernel

  def ask_for_list_name
    print "What is the list name? "
    gets.chomp
  end

  def ask_for_item_data
    print "What is the item called? "
    name = gets.chomp

    print "How much? "
    quantity = gets.chomp.to_i

    [name, quantity]
  end

  def create_list(name)
    { name: name, items: [] }
  end

  def craft_list_item(data)
    { name: data[0], quantity: data[1] }
  end

  def fill_list(list)
    answer = "yes"

    while answer =~ /Y/i
      print "Would you like to add an item to the list? (Y/N) "
      answer = gets.chomp

      if answer =~ /Y/i
        list[:items].push craft_list_item(ask_for_item_data)
      end
    end
  end

  def print_list(list)
    puts "List: #{list[:name]}"
    print_separator

    list[:items].each do |item|
      puts "\tItem: #{item[:name]}\t\t\t Quantity: #{item[:quantity]}"
    end
    print_separator
  end

  def print_separator(character="-")
    puts character * 80
  end

end
